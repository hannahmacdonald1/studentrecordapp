import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controls communication between StudentRecordModel and InsertRecordView
 * @author Brianna Gwilliam
 * @version 1.0
 */
public class InsertRecordController {
    
    private InsertRecordView iView;
    private StudentRecordModel sModel;
    
    public InsertRecordController(InsertRecordView iView, StudentRecordModel sModel) {
        this.sModel = sModel;
        this.iView = iView;
        iView.addPopUpInsertListener(new popUpInsertListener());
        iView.addReturnToMainListener(new returnToMainListener());
    }
    
    
    /**
     * Listener for the "Insert" button in the record insertion window
     * @author Brianna Gwiliam
     */
    class popUpInsertListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String id = "", faculty = "", major = "", year = "";

            
            try {
                id = iView.getId();
                faculty = iView.getFaculty();
                major = iView.getMajor();
                year = iView.getYear();
                
                sModel.insert(id, faculty, major, year);
                
                iView.clearTextFields();

            }
            
            catch(Exception ex) {
                iView.displayErrorMessage("Error!");
            }
            }
        
    }
    
    
    /**
     * Listener for the "Return to Main Window" button in the record insertion window
     * @author Brianna Gwiliam
     */
    class returnToMainListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            iView.clearTextFields();
            iView.closeInsertionWindow();
            
            }
        
    }

}

