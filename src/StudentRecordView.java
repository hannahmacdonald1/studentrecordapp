import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for the StudentRecordApp
 * @author Hannah MacDonald
 * @version 1.0
 */
public class StudentRecordView extends JFrame {
    
    // Center panel elements
    private JTextArea records;
    private JScrollPane recordDisplay;
    
    // North panel elements
    private JLabel title;
    
    // South panel elements
    private JButton insert;
    private JButton find;
    private JButton browse;
    private JButton create;
    
    /**
     * Creates the main window, does NOT make it visible
     * @author Hannah MacDonald
     */
    public StudentRecordView () {
        super("Main Window");
        setSize(600,400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        createRecordPanel();
        createNamePanel();
        createButtonPanel();
    }
    
    /**
     * Creates the record panel and adds it to the frame
     * @author Hannah MacDonald
     */
    private void createRecordPanel() {
        records = new JTextArea(17,50);
        records.setEditable(false);
        recordDisplay = new JScrollPane(records);
        recordDisplay.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        
        JPanel recordPanel = new JPanel();
        recordPanel.add(recordDisplay);
        add(recordPanel, BorderLayout.CENTER);
    }
    
    /**
     * Creates the name panel and adds it to the frame
     * @author Hannah MacDonald
     */
    private void createNamePanel() {
        title = new JLabel("An Application to Maintain Student Records");
        
        JPanel namePanel = new JPanel();
        namePanel.add(title);
        add(namePanel, BorderLayout.NORTH);
    }
    
    /**
     * Creates the button panel and adds it to the frame
     * @author Hannah MacDonald
     */
    private void createButtonPanel() {
        insert = new JButton("Insert");
        find = new JButton("Find");
        browse = new JButton("Browse");
        create = new JButton("Create Tree From File");
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(insert);
        buttonPanel.add(find);
        buttonPanel.add(browse);
        buttonPanel.add(create);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Displays an error dialog.
     * @param errorMessage is the error message.
     */
    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this,errorMessage);
    }
    
    /**
     * Adds a listener to the "Browse" button
     * @author Hannah MacDonald
     */
    public void addBrowseListener (ActionListener listenForBrowseButton) {
        browse.addActionListener(listenForBrowseButton);
    }
    
    /**
     * Adds a listener to the "Create Tree From File" button
     * @author Hannah MacDonald
     */
    public void addCreateListener (ActionListener listenForCreateButton) {
        create.addActionListener(listenForCreateButton);
    }
    
    /**
     * Sets the text in the main text field in the record panel.
     * @param r is the String to put in the text field
     * @author Hannah MacDonald
     */
    public void setRecords(String r) {
        records.setText(r);
    }
    
    /**
     * Adds a listener to the "Find" button
     * @author Muskan Sarvesh
     */
    public void addFindListener(ActionListener listenForFindButton) {
        find.addActionListener(listenForFindButton);
    }
    
    /**
     * Adds a listener to the "Insert" button in the main window
     * @author Brianna Gwilliam
     */
    public void addInsertListener (ActionListener listenForInsertButton) {
        insert.addActionListener(listenForInsertButton);
    }

}

