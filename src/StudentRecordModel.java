import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

/**
 * Contains the data and operations for the StudentRecordApp
 * @author Hannah MacDonald
 * @version 1.0
 */
public class StudentRecordModel {
    
    private BinSearchTree bst;
    
    /**
     * Sets the binary search tree to null.
     */
    public StudentRecordModel() {
        bst = null;
    }
    
    /**
     * Creates the binary search tree given the input file name
     * @author Hannah MacDonald
     * @param fileName is the file Name
     * @throws FileNotFoundException
     */
    public void createTreeFromFile(String fileName) throws FileNotFoundException {

        fileName = fileName.trim();
        Scanner inputFile;
        bst = new BinSearchTree();
        inputFile = new Scanner(new FileReader(fileName));
        
        while (inputFile.hasNextLine()) {
            // Get the next line of the input file
            String line = inputFile.nextLine().trim();
            
            // Split each value into an array based on any number of whitespaces
            String [] parameters = line.split("\\s+");
            
            // Insert the data into the tree
            bst.insert(parameters[0], parameters[1], parameters[2], parameters[3]);
        }
        
        inputFile.close();
        
    }
    
    /**
     * Prints the search tree to a string.
     * @author Hannah MacDonald
     * @return the elements of the binary search tree as a string
     * @throws IOException
     */
    public String printTreeToString() throws IOException {
        StringWriter s = new StringWriter();
        bst.print_tree(bst.root, new PrintWriter(s));
        String r = s.toString();
        return r;
    }
    
    public BinSearchTree getBst() {
        return bst;
    }

    /**
     * Searches for the student id in the binary search tree and returns
     * the information of that student.
     * @author Muskan Sarvesh
     * @param target_id: is the student id which needs to be searched
     * @return a string with the details of the student searched
     */
    public String findRecord(String target_id) {
        // TODO Auto-generated method stub
        Node found = bst.find(bst.root, target_id);
        String record = found.toString();
        return record;
    }
    

    /**
     * Inserts a student with the specified information into the binary
     * search tree.
     * @author Brianna Gwilliam
     * @param id the student's ID
     * @param faculty the student's faculty
     * @param major the student's major
     * @param year the student's year of study
     */
    public void insert(String id, String faculty, String major, String year) {
        bst.insert(id, faculty, major, year);
    }
    
    
    
}
