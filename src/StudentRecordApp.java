/**
 * An app that creates student records from an input text file,stores them in a
 * binary search tree, and allows the user to browse, find, or add records.
 * @author Hannah MacDonald
 * @version 1.0
 */
public class StudentRecordApp {

    public static void main (String [] args) {
        StudentRecordModel myModel = new StudentRecordModel();
        StudentRecordView myView = new StudentRecordView();
        
        InsertRecordView myInsertionView = new InsertRecordView();
        
        StudentRecordController myController = new StudentRecordController(myView,myModel, myInsertionView);
        
        InsertRecordController myInsertionController = new InsertRecordController(myInsertionView,myModel);
        
        myView.setVisible(true);
        
    }
}
