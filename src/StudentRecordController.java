import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 * Controls communication between StudentRecordModel and StudentRecordView
 * @author Hannah MacDonald
 * @version 1.0
 */
public class StudentRecordController {
    
    private StudentRecordView sView;
    private StudentRecordModel sModel;
    private InsertRecordView iView;
    
    public StudentRecordController(StudentRecordView sView, StudentRecordModel sModel, InsertRecordView iView) {
        this.sModel = sModel;
        this.sView = sView;
        this.iView = iView;
        sView.addCreateListener(new createListener());
        sView.addBrowseListener(new browseListener());
        sView.addFindListener(new findListener());
        sView.addInsertListener(new insertListener());
    }
    
    /**
     * Listener for the "Create Tree From File" button
     * @author Hannah MacDonald
     */
    class createListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
            String fileName = JOptionPane.showInputDialog("Enter the file name:");
            sModel.createTreeFromFile(fileName);
            } catch (FileNotFoundException e1) {
                sView.displayErrorMessage("Could not open input file.");
            }
        }
        
    }
    
    
    /**
     * Listener for the "Browse" button
     * @author Hannah MacDonald
     */
    class browseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if(sModel.getBst()!=null)
                    sView.setRecords(sModel.printTreeToString());
                else
                    sView.displayErrorMessage("There is no tree to display. Create the tree first");
            } catch (IOException e1) {
                sView.displayErrorMessage("Could not display records.");
            } catch (Exception e1) {
                sView.displayErrorMessage("Error!");
            }
        }
        
    }
    
    /**
     * Listener for the "Find" button
     * @author Muskan Sarvesh
     */
    class findListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String id = JOptionPane.showInputDialog("Please enter the student's id:");
                String record = sModel.findRecord(id);
                JOptionPane.showMessageDialog(null, record, "Message", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception ex) {
                sView.displayErrorMessage("This student does not exist.");
            }
        }
    }
    
    
    
      /**
     * Listener for the "Insert" button in the main window
     * @author Brianna Gwiliam
     */
    class insertListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                    if(sModel.getBst()!=null)
                        iView.showInsertionWindow();
                    else
                        sView.displayErrorMessage("There is no tree to display. Create the tree first");
                } catch (Exception e1) {
                    sView.displayErrorMessage("Error!");
                }
            }
        
        
    }

}

