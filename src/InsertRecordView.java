import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for the StudentRecordApp
 * @author Brianna Gwilliam
 * @version 1.0
 */
public class InsertRecordView extends JFrame {
    
    //south panel elements
    private JButton popUpInsert;
    private JButton returnToMain;
    
    //center panel elements
    private JTextField id = new JTextField(10);
    private JTextField faculty = new JTextField(10);
    private JTextField major = new JTextField(10);
    private JTextField year = new JTextField(10);
    private JLabel idLabel = new JLabel("Enter the Student ID");
    private JLabel facultyLabel = new JLabel("Enter Faculty");
    private JLabel majorLabel = new JLabel("Enter Student's Major");
    private JLabel yearLabel = new JLabel("Enter year");
    
    
    /**
     * Creates the insertion window, does NOT make it visible
     * @author Brianna Gwilliam
     */
    public InsertRecordView () {
        super();
        setSize(500,180);
        
        createButtonPanel();
        createNamePanel();
        createInputPanel();
    
    }
    
    
    private void createButtonPanel() {
       popUpInsert = new JButton("Insert");
        returnToMain = new JButton("Return to Main Window");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(popUpInsert);
        buttonPanel.add(returnToMain);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    
    private void createNamePanel() {
       JLabel title = new JLabel("Insert a New Node");
       JPanel namePanel = new JPanel();
       namePanel.add(title);
       add(namePanel, BorderLayout.NORTH);
    }
    
    
    private void createInputPanel() {
       JPanel inputPanel = new JPanel();
       inputPanel.add(idLabel);
       inputPanel.add(id);
       inputPanel.add(facultyLabel);
       inputPanel.add(faculty);
       inputPanel.add(majorLabel);
       inputPanel.add(major);
       inputPanel.add(yearLabel);
       inputPanel.add(year);
       add(inputPanel, BorderLayout.CENTER);
    }
    
    
    /**
     * Adds a listener to the "Insert" button in the record insertion window
     * @author Brianna Gwilliam
     */
   public void addPopUpInsertListener (ActionListener listenForPopUpInsertButton) {
        popUpInsert.addActionListener(listenForPopUpInsertButton);
   }
    
    
    /**
         * Adds a listener to the "Return to Main Window" button in the record insertion window
         * @author Brianna Gwilliam
         */
       public void addReturnToMainListener (ActionListener listenForReturnToMainButton) {
            returnToMain.addActionListener(listenForReturnToMainButton);
    }
    
    
    /**
     * Gets the ID number from the associated text field
     *@author Brianna Gwilliam
     *@return the ID number
     */
    public String getId() {
        return id.getText();
    }
    
    /**
     * Gets the faculty from the associated text field
     *@author Brianna Gwilliam
     *@return the faculty
     */
    public String getFaculty() {
        return faculty.getText();
    }
    
    /**
     * Gets the major from the associated text field
     *@author Brianna Gwilliam
     *@return the major
     */
    public String getMajor() {
       return major.getText();
    }
    
    /**
     * Gets the year from the associated text field
     *@author Brianna Gwilliam
     *@return the year
     */
    public String getYear() {
        return year.getText();
    }
    
    /**
     * Resets all previous input from the text fields
     *@author Brianna Gwilliam
     */
    public void clearTextFields() {
        id.setText("");
        faculty.setText("");
        major.setText("");
        year.setText("");
    }
    
    /**
     * Stops displaying the window to insert a new student record into
     * the tree
     *@author Brianna Gwilliam
     */
    public void closeInsertionWindow() {
        setVisible(false);
    }
    
    /**
     * Displays window for entering student information to be inserted
     * into the tree
     * @author Brianna Gwilliam
    */
       public void showInsertionWindow() {
           setVisible(true);
       }

    

/**
 * Displays an error dialog.
 * @param errorMessage is the error message.
 */

    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }


}